#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>


        void imprimeVetorInt(int v[], int nel){
            int n;
            for(n=0; n< nel; n++){
                printf("%d, ", v[n]);
            }
        }

       void ExercicioDois()
        {
            char frase[50];
            char vogais[] = "aeiou";
            int n,x;
            system("cls");
            setbuf(stdin,NULL); //limpa o buffer do teclado
            printf("Digite uma frase: \n");
            gets(frase);
            for (n = 0; n < strlen(frase) ; n++)
            {
                for (x = 0; x < strlen(vogais); x++)
                {
                    if ((toupper(frase[n]) == (toupper(vogais[x]))))
                    {
                        printf("%c, ", frase[n]);
                    }
                }
            }
            printf("\n\n\n");
            setbuf(stdin,NULL); //limpa o buffer do teclado
            system("pause");
        }

       void ExercicioTres()
        {
            int a[] = { 1, 2, 3, 4, 5 };
            int b[] = { 11, 3, 50, 6, 1, 8, 5, 10 };
            int n,m;

            system("cls");
            printf("Vetor A: ");
            for (n = 0; n < sizeof(a)/sizeof(int); n++)
            {
                printf("%d, ", a[n]);
            }
            printf("\n");
            printf("Vetor B: ");
            for (n = 0; n < sizeof(b)/sizeof(int); n++)
            {
                printf("%d, ", b[n]);
            }
            printf("\n");
            printf("Elementos em comum: ");
            for (n = 0; n < sizeof(a)/sizeof(int); n++)
            {
                for (m = 0; m < sizeof(b)/sizeof(int); m++)
                {
                    if (a[n] == b[m])
                        printf("%d, ", a[n]);
                }
            }
            printf("\n\n");
            system("pause");
        }

        void ExercicioQuatro()
        {
            int binario[] = {1,0,0,1};
            int valDec = 0;
            int valor = 0;
            int n = 0;
            int pos;

            system("cls");
            for (n = (sizeof(binario)/sizeof(int))-1, pos = 0; n >= 0; n--, pos++)
            {
                valor = ((int)pow(2, pos)) * (binario[n]);
                valDec += valor;
            }

            printf("\n\nValor binário : ");
            imprimeVetorInt(binario,4);
            printf("\nValor decimal : %d", valDec);
            printf("\n\n");
            system("pause");
        }



        void Menu()
        {

            int opcao = 0;
            int erro = 0;
            do
            {
                system("cls");
                printf("Exercícios sobre Vetor\n\n");
                printf("1) Solução do exercício 1\n");
                printf("2) Solução do exercício 2\n");
                printf("3) Solução do exercício 3\n");
                printf("4) Solução do exercício 4\n");
                printf("0) S A I R\n\n");
                if (erro)
                {
                    printf("Opção inválida!\n");
                }
                erro = 0;
                printf("Faça uma escolha: ");
                scanf("%d",&opcao);
                setbuf(stdin,NULL); //limpa o buffer do teclado
                switch (opcao)
                {
                    case 0:
                        return;
                    case 1:
                        break;
                    case 2:
                        ExercicioDois();
                        break;
                    case 3:
                        ExercicioTres();
                        break;
                    case 4:
                        ExercicioQuatro();
                        break;
                    default:
                        erro = 1;
                        break;
                }

            } while (1);

        }

int main()
{
    setlocale(LC_ALL,"Portuguese");
    Menu();
    return 0;
}
